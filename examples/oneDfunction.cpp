
#include <tclap/CmdLine.h>
#include <Eigen/Core>
#include <random>
#include "netcdfDatabase.h"
#include "network.h"
#include "functions.h"
#include "baseOptimizer.h"
#include "readMNIST.h"
#include "timer.h"
#include "layerCrossEntropy.h"

using namespace TCLAP;
using namespace std;

/*
working with mnist data
*/
int main(int argc, char*argv[])
{
/*
    Eigen::MatrixXd a(3,2);
    a << 1,2,3,2.2,3.3,1.1;
    cout << a << endl << endl;
    Eigen::MatrixXd zmax(1,2);
    zmax =a.colwise().maxCoeff();
    cout <<zmax  << endl << endl;
    for (int cc = 0; cc < a.cols(); ++cc)
        {
        a.col(cc) = (a.col(cc).array()-zmax(0,cc)).exp();
        a.col(cc) /= a.col(cc).sum();
        }
    cout <<a  << endl << endl;
*/

    // wrap tclap in a try block
    try
    {
    //First, we set up a basic command line parser...
    //      cmd("command description message", delimiter, version string)
    CmdLine cmd("basic testing of simpleNeuralNet", ' ', "V0.0");

    //define the various command line strings that can be passed in...
    //ValueArg<T> variableName("shortflag","longFlag","description",required or not, default value,"value type",CmdLine object to add to
    ValueArg<int> programSwitchArg("z","programSwitch","an integer controlling program branch",false,0,"int",cmd);
    ValueArg<int> itSwitchArg("i","iterations","number of minibatches",false,100,"int",cmd);
    ValueArg<int> batchSizeSwitchArg("m","batchSize","number of samples per gradient computation",false,1,"int",cmd);
    ValueArg<float> learningRateSwitchArg("e","learningRate","learning rate",false,0.01,"float",cmd);
    ValueArg<float> regularizeSwitchArg("r","regularization","regularization strength",false,0.0,"float",cmd);
    //parse the arguments
    cmd.parse( argc, argv );

    int programSwitch = programSwitchArg.getValue();
    int iterations = itSwitchArg.getValue();
    int batchSize = batchSizeSwitchArg.getValue();
    double learningRate = learningRateSwitchArg.getValue();
    double regularize = regularizeSwitchArg.getValue();
    timer readTime,trainTime,testTime,fullGradientTime;

    string mnistTrain = "/Users/dmsussma/data/mnist/train-images-idx3-ubyte";
    string mnistTrainLabel = "/Users/dmsussma/data/mnist/train-labels-idx1-ubyte";
    string mnistTest = "/Users/dmsussma/data/mnist/t10k-images-idx3-ubyte";
    string mnistTestLabel = "/Users/dmsussma/data/mnist/t10k-labels-idx1-ubyte";

    vector<double> trainingLabels;
    vector<double>  trainingVectors;
    vector<double> testLabels;
    vector<double>  testVectors;
    readTime.start();
    readMnistLabels(mnistTrainLabel, trainingLabels);
    readMnist(mnistTrain, trainingVectors);
    readMnistLabels(mnistTestLabel, testLabels);
    readMnist(mnistTest, testVectors);
    readTime.stop();
    //printf("we have %lu labeled examples loaded and %lu vectors\n",trainingLabels.size(),trainingVectors.size());
    int nDigits=10;
    int nCols = trainingLabels.size()/nDigits;
    int nRows = trainingVectors.size()/nCols;

    int nTestCols = testLabels.size()/nDigits;
    int nTestRows = testVectors.size()/nTestCols;
    readTime.printWithMessage("loading took");

    Eigen::MatrixXd input = (Eigen::Map<Eigen::MatrixXd>(&testVectors[0],nTestRows,nTestCols))/255.;
    Eigen::MatrixXd target = (Eigen::Map<Eigen::MatrixXd>(&testLabels[0],nDigits,nTestCols));
    Eigen::MatrixXd train = (Eigen::Map<Eigen::MatrixXd>(&trainingVectors[0],nRows,nCols))/255.;
    Eigen::MatrixXd labels = (Eigen::Map<Eigen::MatrixXd>(&trainingLabels[0],nDigits,nCols));


    std::vector<int> nodesPerLayer{nRows,200,200,10,10,10};
    //std::vector<int> nodesPerLayer{nRows,10,100,10,100,10,10};
    //std::vector<int> nodesPerLayer{nRows,10,10,10,10,10,10};
    std::vector<shared_ptr<layer> > ls;
    for (int ii = 1; ii < nodesPerLayer.size()-1;++ii)
        {
        if(ii == nodesPerLayer.size()-2)
            ls.push_back(make_shared<layerCrossEntropy>(nodesPerLayer[ii-1],nodesPerLayer[ii],nodesPerLayer[ii+1]));
        else
            ls.push_back(make_shared<layer>(nodesPerLayer[ii-1],nodesPerLayer[ii],nodesPerLayer[ii+1]));
        }

    network net;
    for (int ii = 0; ii < ls.size(); ++ii)
        net.addLayer(ls[ii]);


    int nParams = net.getNumberOfParameters();
    char dataname[256];
    if(programSwitch > 1)
        sprintf(dataname,"../data/AdaptiveBatch_test_N%i_eta%f.nc",nParams,learningRate);
    else
        sprintf(dataname,"../data/test_N%i_m%i_eta%f.nc",nParams,batchSize,learningRate);
    databaseNetCDF ncdat(nParams,dataname,NcFile::Replace);

    double cost,fullCost, mean,var,Teff,accuracy,maxGradient;
    std::vector<double> X,DX;
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<> uni(0,nCols-1);

    baseOptimizer optimize(nParams,learningRate);
    Eigen::MatrixXd miniBatchInput(nRows,batchSize);
    Eigen::MatrixXd miniBatchTarget(nDigits,batchSize);
    trainTime.start();
    int logSaveIdx = 0;
    int nextSave = 0;
    for(int jj = 0; jj < iterations; ++jj)
        {
        if(miniBatchInput.cols() != batchSize)
            {
            miniBatchInput.resize(nRows,batchSize);
            miniBatchTarget.resize(nDigits,batchSize);
            }

        for (int mm = 0; mm < batchSize; ++mm)
            {
            int smpl = uni(mt);
            miniBatchInput.col(mm) = train.col(smpl);
            miniBatchTarget.col(mm) =labels.col(smpl);
            }

        net.getParametersGradientAndCost(miniBatchInput,miniBatchTarget,optimize.x,optimize.gradient,cost);

        if((jj==iterations-1)||(programSwitch ==0 &&jj%100 ==0)|| (programSwitch>0 && jj==nextSave) )
            {
            if(programSwitch > 0)
                {
                nextSave = 10*(int)round(pow(pow(10.0,0.05),logSaveIdx));
                while(nextSave == jj)
                    {
                    logSaveIdx +=1;
                    nextSave = 10*(int)round(pow(pow(10.0,0.05),logSaveIdx));
                    };
                };
            fullGradientTime.start();
            net.getParametersGradientAndCost(train,labels,X,DX,fullCost);
            vectorSum(DX,optimize.gradient,-1.0);
            vectorMeanVariance(DX,mean,var);
            Teff = var * learningRate *0.5;
            fullGradientTime.stop();
            maxGradient = vectorMaximumComponentMagnitude(DX);
            accuracy=net.validationAccuracy(input, target);
            printf("time:%i\t True cost:%f\t Delta cost:%f\t var: %g\t acc:%.3f\t batch size:%i\n",
                         jj,fullCost,cost-fullCost,var,accuracy,batchSize);
            ncdat.writeNetwork(optimize.x, 1.0-accuracy,fullCost, var, maxGradient,jj*learningRate);
            if(programSwitch > 1) batchSize += programSwitch;
            if (batchSize > nCols/5) batchSize = nCols/5;
            }
        //if(jj%100000==99999) optimize.eta /= 10.;
        //optimize.eta = learningRate/(1.0+learningRate*0.01*jj);
        optimize.update();
        net.setParameters(optimize.x);
       //if(jj%10000==0) printf("(%i,%g)\t",jj,cost);
        }
    trainTime.stop();
    trainTime.printWithMessage("training took");
    fullGradientTime.printWithMessage("full gradient calculations take ~ ");

    testTime.start();
    double correct = net.validationAccuracy(input, target);
    testTime.stop();
    testTime.printWithMessage("testing took");
    std::cout << "final accuracy of " << correct << " using " << nParams << " total parameters" << endl;
    //net.evaluate(input,target);


//
//The end of the tclap try
//
    } catch (ArgException &e)  // catch any exceptions
    { cerr << "error: " << e.error() << " for arg " << e.argId() << endl; }
    return 0;
};
