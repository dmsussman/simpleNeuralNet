#!/bin/bash

z=1
i=1000000
replicas=10;
for layerSize in 5 10 20 40 80
do
    for m in 4 16 64
    do
        ./test.out "-z" $z "-i" $i "-m" $m "-r" $replicas "-l" $layerSize > /dev/null 2>&1 &
    done
    wait
    echo $layerSize
done
