# simpleNeuralNet

Sometimes it's good to check your understanding by building things yourself...
here's a set of tools that can build extremely simple fully connected neural nets, train them, blah blah blah. 

As a toy problem, we study the ``effective temperature'' of under- and over-parameterized NNs as a function of training schedule, etc., for MNIST and CIFAR-10. An interesting result is obtained: naive (but typical?) training schedules for underparameterized nets correspond to an annealing schedule which is the opposite of what one would want in preparing a glassy state.

Sub-directory info:
* src/network contains classes for different types of network layers, together with a network class which ties everything together
* src/optimizers contains classes for different optimization methods (currently just first order overdamped, athermal dynamics)
* src/utility, shockingly, contains utility functions and classes (saving/loading networks, reading MNIST and CIFAR-10 datasets, vector operations, etc.)
* src/tclap is a straight copy of the templatized c++ command line parser library
* examples/ is where I'll keep alternate main.cpp files, by default the cmake compilation scheme will build from "simpleNeuralNet.cpp". Easy to change, obviously.




## Basic compilation

Have at least Eigen3 installed, then...

* $ cd build/
* $ cmake ..
* $ make

Wow.
