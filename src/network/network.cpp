#include "network.h"

network::network()
    {
    }

void network::addLayer(std::shared_ptr<layer> l)
    {
    layers.push_back(l);
    }

void network::debugEvaluate(matrix &input, matrix &target)
    {
    int inputs = input.cols();
    propagate(input);
    double cost = (1.0/inputs)*((target-layers[layers.size()-1]->activation).colwise().squaredNorm()).sum();
    //std::cout << "input is " << input << std::endl;
    std::cout << "target is " << target << std::endl;
    std::cout << "answer is " << layers[layers.size()-1]->activation << std::endl;
    std::cout << "cost is " << cost << std::endl;
    }

double network::validationAccuracy(matrix &input, matrix &target)
    {
    propagate(input);
    matrix networkAnswer = layers[layers.size()-1]->activation;
    double correct = 0.0;
    Eigen::MatrixXf::Index maxIndex;
    for (int ss = 0; ss <  target.cols(); ++ss)
        {
        target.col(ss).maxCoeff(&maxIndex);
        int truth = maxIndex;
        networkAnswer.col(ss).maxCoeff(&maxIndex);
        int prediction = maxIndex;
        if(truth==prediction) correct+=1.0;
        }
    correct /= target.cols();
    return correct;
    }

void network::setParameters(std::vector<double> &x)
    {
    int curSize = 0;
    for (int ll = 0; ll < layers.size();++ll)
        {
        std::vector<double> p(layers[ll]->nParameters);
        std::copy(x.begin()+curSize,x.begin()+curSize+p.size(),p.begin());
        layers[ll]->setParameters(p);
        curSize += p.size();
        }
    }
int network::getNumberOfParameters()
    {
    int totalSize = 0;
    for (int ll = 0; ll < layers.size();++ll)
        totalSize +=layers[ll]->nParameters;
    return totalSize;
    }

void network::getParametersGradientAndCost(matrix &input,matrix &target,
            std::vector<double> &x,std::vector<double> &dx,double &cost)
    {
    propagate(input);
    backpropagate(input,target,cost);

    int totalSize = getNumberOfParameters();
    x.resize(totalSize);
    dx.resize(totalSize);
    std::vector<double> tempx,tempdx;
    int curSize = 0;
    for(int ll = 0; ll < layers.size();++ll)
        {
        layers[ll]->getParameters(tempx);
        layers[ll]->getGradient(tempdx);
        std::copy(tempx.begin(),tempx.end(),x.begin()+curSize);
        std::copy(tempdx.begin(),tempdx.end(),dx.begin()+curSize);
        curSize += layers[ll]->nParameters;
        }
    }

void network::getParametersAndGradients(std::vector<double> &x,std::vector<double> &dx)
    {
    int totalSize = getNumberOfParameters();
    x.resize(totalSize);
    dx.resize(totalSize);
    std::vector<double> tempx,tempdx;
    int curSize = 0;
    for(int ll = 0; ll < layers.size();++ll)
        {
        layers[ll]->getParameters(tempx);
        layers[ll]->getGradient(tempdx);
        std::copy(tempx.begin(),tempx.end(),x.begin()+curSize);
        std::copy(tempdx.begin(),tempdx.end(),dx.begin()+curSize);
        curSize += layers[ll]->nParameters;
        }
    }

void network::propagate(matrix &input)
    {
    layers[0]->forward(input);
    for(int ll = 1; ll < layers.size(); ++ll)
        {
        layers[ll]->forward(layers[ll-1]->activation);
        }
    };

void network::backpropagate(matrix &input,matrix &target, double &cost)
    {
    if(layers.size()>1)
        {
        //the last layer evaluates the cost function
        layers[layers.size()-1]->backwards(layers[layers.size()-2]->activation,target, target,cost,true);
        for(int ll = layers.size()-2;ll>0; --ll)
            layers[ll]->backwards(layers[ll-1]->activation,layers[ll+1]->weights,layers[ll+1]->delta,cost,false);
        //the first hidden layer is activated by the input
        layers[0]->backwards(input,layers[1]->weights,layers[1]->delta,cost,false);
        }
    else
        throw std::runtime_error("just one layer... case not covered yet for some reason");
    };
