#include "layer.h"
#include <random>
using namespace std;

layer::layer(int lastLayerSize, int curLayerSize, int nextLayerSize):
    nLast(lastLayerSize), n(curLayerSize), nNext(nextLayerSize)
    {
    weights.resize(n,nLast);
    dWeights.resize(n,nLast);
    bias.resize(n,1);
    dBias.resize(n,1);
    xavierInitialize();
    nParameters = weights.size()+bias.size();
    };

void layer::xavierInitialize()
    {
    std::random_device rd;
    std::mt19937 mt(rd());
    std:: normal_distribution<> normal(0,pow(2.0/(nLast+nNext),.5));
    for(int ii = 0; ii < n; ++ii)
        for (int jj = 0; jj < nLast; ++jj)
            weights(ii,jj)=normal(mt);
    for(int ii = 0; ii < n; ++ii)
        bias(ii) =normal(mt);
    }


double activationFunction(double x) // the functor we want to apply
    {
    return tanh(x);
    }

double activationFunctionDerivative(double x) // the functor we want to apply
    {
    double sech = 1.0/cosh(x);
    return sech*sech;
    }

//feed the layer the previous layer's activation
void layer::forward(matrix &previousLayer)
    {
    if(activation.rows() != weights.rows() || activation.cols() != previousLayer.cols())
        {
        z.resize(weights.rows(),previousLayer.cols());
        activation.resize(weights.rows(),previousLayer.cols());
        }
    z = weights*previousLayer;
    z.colwise() += bias;
    activation=z.unaryExpr(&activationFunction);
    }

//if finalLayer, "nextLayerDelta" should just be the ground truth
void layer::backwards(matrix &lastLayerActivation ,matrix &nextLayerWeights,
            matrix &nextLayerDelta, double &cost, bool finalLayer)
    {
    int m = lastLayerActivation.cols();
    if(finalLayer)
        {
        //cost is average of 0.5(output-truth)^2
        cost=(0.5/(double)m)*((activation-nextLayerDelta).colwise().squaredNorm()).sum();
        delta = (activation-nextLayerDelta).cwiseProduct(z.unaryExpr(&activationFunctionDerivative));
        }
    else
        {
        delta = ((nextLayerWeights.transpose())*nextLayerDelta)
                        .cwiseProduct(z.unaryExpr(&activationFunctionDerivative));
        }

    dWeights = delta*(lastLayerActivation.transpose());
    dWeights /= m;
    dBias = delta.rowwise().mean();
    }

void layer::getParameters(std::vector<double> &p)
    {
    // Copy the data of weights and bias to the vector
    p.resize(nParameters);
    std::copy(weights.data(),weights.data()+weights.size(),p.begin());
    std::copy(bias.data(),bias.data() + bias.size(),p.begin()+weights.size());
    }

void layer::setParameters(std::vector<double> &p)
    {
    if(p.size()!=nParameters)
        throw std::invalid_argument("layer parameter size vector does not match weight and bias sizes");
    std::copy(p.begin(),p.begin()+weights.size(),weights.data());
    std::copy(p.begin()+weights.size(),p.end(),bias.data());
    }

void layer::getGradient(std::vector<double> &g)
    {
    g.resize(nParameters);
    std::copy(dWeights.data(),dWeights.data()+dWeights.size(),g.begin());
    std::copy(dBias.data(),dBias.data() + dBias.size(),g.begin()+dWeights.size());
    }
