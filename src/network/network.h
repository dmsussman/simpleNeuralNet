#ifndef NETWORK_H
#define NETWORK_H

#include "std_include.h"
#include "layer.h"

class network
    {
    public:
        network();

        std::vector<std::shared_ptr<layer> > layers;

        void propagate(matrix &input);

        void backpropagate(matrix &input,matrix &target,double &cost);

        void getParametersGradientAndCost(matrix &input,matrix &target,
                        std::vector<double> &x,std::vector<double> &dx,
                        double &cost);

        void addLayer(std::shared_ptr<layer> l);

        void getParametersAndGradients(std::vector<double> &x,std::vector<double> &dx);
        void setParameters(std::vector<double> &x);

        void debugEvaluate(matrix &input, matrix &target);

        double validationAccuracy(matrix &input, matrix &target);

        int getNumberOfParameters();

        void printActivations()
            {
            for (int ll =0; ll <layers.size();++ll)
                    std::cout << "layer " << ll << std::endl << layers[ll]->activation << std::endl << std::endl;
            }

    };

#endif
