#include "layerCrossEntropy.h"
#include <random>
using namespace std;

layerCrossEntropy::layerCrossEntropy(int lastLayerSize, int curLayerSize, int nextLayerSize):
    layer(lastLayerSize,curLayerSize,nextLayerSize)
    {
    zmax.resize(1,1);
    };

//feed the layer the previous layer's activation; output is softmax
void layerCrossEntropy::forward(matrix &previousLayer)
    {
    if(activation.rows() != weights.rows() || activation.cols() != previousLayer.cols())
        {
        z.resize(weights.rows(),previousLayer.cols());
        activation.resize(weights.rows(),previousLayer.cols());
        zmax.resize(1,previousLayer.cols());
        }
    z = weights*previousLayer;
    z.colwise() += bias;
    zmax =z.colwise().maxCoeff();
    for (int cc = 0; cc < activation.cols(); ++cc)
        {
        activation.col(cc) = (z.col(cc).array()-zmax(0,cc)).exp();
        activation.col(cc) /= activation.col(cc).sum();
        }
    }

//if finalLayer, "nextLayerDelta" should just be the ground truth
void layerCrossEntropy::backwards(matrix &lastLayerActivation ,matrix &nextLayerWeights,
            matrix &nextLayerDelta, double &cost, bool finalLayer)
    {
    int m = lastLayerActivation.cols();
    if(finalLayer)
        {
        //cost is average of  -\sum_i truth_i log(activation_i)
        cost = (-1.0/(double)m)*(nextLayerDelta.cwiseProduct(((activation.array()).log()).matrix())).sum();
        delta = (activation-nextLayerDelta);
        }
    else
        {
        throw std::runtime_error("softmax only at the final layer, at the moment...");
        }
    dWeights = delta*(lastLayerActivation.transpose());
    dWeights /= m;
    dBias = delta.rowwise().mean();
    }
