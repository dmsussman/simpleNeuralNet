#ifndef LAYER_H
#define LAYER_H

#include "std_include.h"

/*
The basic layer is fully connected and uses .5(x-y)^2 as the cost function in the final layer
*/
class layer
    {
    public:
        layer(int lastLayerSize, int curLayerSize, int nextLayerSize);

        int nLast;
        int nNext;
        int n;
        int nParameters;

        matrix weights;
        matrix dWeights;
        matrix activation;
        matrix z;
        matrix delta;
        dVector bias;
        dVector dBias;

        virtual void forward(matrix &previousLayer);
        virtual void backwards(matrix &lastLayerActivation,matrix &nextLayerWeights,
                    matrix &nextLayerDelta, double &cost, bool finalLayer);

        virtual void getParameters(std::vector<double> &p);
        virtual void setParameters(std::vector<double> &p);
        virtual void getGradient(std::vector<double> &g);


    protected:
        void xavierInitialize();

    };

#endif
