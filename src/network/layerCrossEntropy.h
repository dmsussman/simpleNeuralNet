#ifndef LAYERCROSSENTROPY_H
#define LAYERCROSSENTROPY_H

#include "layer.h"

/*
Fully connected and uses softmax and cross entropy as the cost function in the final layer
*/
class layerCrossEntropy : public layer
    {
    public:
        layerCrossEntropy(int lastLayerSize, int curLayerSize, int nextLayerSize);

        virtual void forward(matrix &previousLayer);
        virtual void backwards(matrix &lastLayerActivation,matrix &nextLayerWeights,
                    matrix &nextLayerDelta, double &cost, bool finalLayer);
        matrix zmax;
    };

#endif
