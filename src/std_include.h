#ifndef STD_INCLUDE_H
#define STD_INCLUDE_H

#include <Eigen/Core>
#include <iostream>
#include <vector>
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> matrix;
typedef Eigen::RowVectorXi iVector;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> dVector;

inline void printSize(matrix &a)
{
    std::cout << "matrix has " << a.rows() << " rows and " << a.cols() << "cols" << std::endl;
}

#define PI  3.14159265358979323846

//spot-checking of code for debugging
#define DEBUGCODEHELPER printf("\nReached: file %s at line %d\n",__FILE__,__LINE__);

#endif
