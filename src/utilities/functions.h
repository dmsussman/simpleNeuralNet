#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <vector>

using namespace std;

//the naive algorithm, not an especially numerically stable one
void vectorMeanVariance(vector<double> &a, double &mean, double &var);

//\vec{a} = \vec{a} + \vec{b}
void vectorSum(vector<double> &a, const vector<double> &b);

//\vec{a} = \vec{a} + mult*\vec{b}
void vectorSum(vector<double> &a, const vector<double> &b,double mult);

double vectorMaximumComponentMagnitude(const vector<double> &a);

#endif
