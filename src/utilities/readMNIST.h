#ifndef READMNIST_H
#define READMNIST_H

#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>

using namespace std;

int reverseInt(int i);

void readMnist(string filename, vector<double> &vec);

void readMnistLabels(string filename, vector<double> &vec);

#endif
