#include "functions.h"
#include <math.h>
double vectorMaximumComponentMagnitude(const vector<double> &a)
    {
    double ans =0.0;
    for (int ii = 0; ii < a.size();++ii)
        if(fabs(a[ii]) > ans)
            ans = fabs(a[ii]);
    return ans;
    }

void vectorSum(vector<double> &a, const vector<double> &b)
    {
    for (int ii = 0; ii < a.size();++ii)
        a[ii]=a[ii]+b[ii];
    }

void vectorSum(vector<double> &a, const vector<double> &b,double mult)
    {
    for (int ii = 0; ii < a.size();++ii)
        a[ii]=a[ii]+mult*b[ii];
    }

void vectorMeanVariance(vector<double> &a, double &mean, double &var)
    {
    //"mean" will hold the mean, var will temporarily hold <x^2>
    mean = 0.0;
    var = 0.0;
    for(int ii = 0; ii < a.size(); ++ii)
        {
        mean += a[ii];
        var += a[ii]*a[ii];
        }
    mean /= a.size();
    var /= a.size();
    var -= mean*mean;
    }
