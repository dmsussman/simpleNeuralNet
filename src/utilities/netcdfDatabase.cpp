#include "netcdfDatabase.h"

databaseNetCDF::databaseNetCDF(int nParams,std::string fn, NcFile::FileMode mode)
     : filename(fn), Mode(mode),records(0), File(fn.c_str(), mode), N(nParams)
    {
    NcError err(NcError::silent_nonfatal);
    switch(Mode)
        {
        case NcFile::ReadOnly:
            getDimVar();
            break;
        case NcFile::Write:
            getDimVar();
            break;
        case NcFile::Replace:
            setDimVar();
            break;
        case NcFile::New:
            setDimVar();
            break;
        default:
            ;
        };
    }

void databaseNetCDF::setDimVar()
    {
    //Set the dimensions
    recDim = File.add_dim("rec");
    NDim  = File.add_dim("N",  N);
    unitDim = File.add_dim("unit",1);

    //Set the variables
    paramVar = File.add_var("parameterValues",   ncDouble,recDim, NDim);
    errorVar = File.add_var("error", ncDouble,recDim, unitDim);
    costVar = File.add_var("cost", ncDouble,recDim, unitDim);
    varianceVar = File.add_var("gradientVariance", ncDouble,recDim, unitDim);
    maxGradVar = File.add_var("maxGrad",ncDouble,recDim,unitDim);
    timeVar  = File.add_var("time", ncDouble,recDim, unitDim);
    }

void databaseNetCDF::getDimVar()
    {
    //Get the dimensions
    recDim = File.get_dim("rec");
    NDim  = File.get_dim("N");
    unitDim = File.get_dim("unit");
    //Get the variables
    paramVar = File.get_var("parameterValues");
    errorVar = File.get_var("error");
    costVar = File.get_var("cost");
    varianceVar = File.get_var("gradientVariance");
    maxGradVar = File.get_var("maxGrad");
    timeVar  = File.get_var("time");
    }

void databaseNetCDF::writeNetwork(std::vector<double> &params, double error,
                        double cost, double variance, double maxGradientComponent,double time, int rec)
    {
    if(rec<0) rec = recDim->size();
    if (time < 0) time = rec;

    paramVar->put_rec(&params[0], rec);
    errorVar->put_rec(&error, rec);
    costVar->put_rec(&cost, rec);
    varianceVar->put_rec(&variance, rec);
    maxGradVar->put_rec(&maxGradientComponent,rec);
    timeVar->put_rec(&time,     rec);

    File.sync();
    };
