#ifndef netcdfDATABASE_H
#define netcdfDATABASE_H

#include <netcdfcpp.h>
#include "std_include.h"

/*!
BaseDatabase just provides an interface to a file and a mode of operation.
*/
class databaseNetCDF
    {
    public:
        //!The NcFile itself
        NcFile File;

        //!The default constructor starts a bland filename in readonly mode
        databaseNetCDF(int nParams, std::string fn="temp.nc", NcFile::FileMode mode=NcFile::ReadOnly);

        //!The name of the file
        std::string filename;
        //!The desired mode (integer representation of replace, new, write, readonly, etc)
        const int Mode;
        int N;

        NcDim *recDim, *NDim, *unitDim;
        NcVar *paramVar, *errorVar, *costVar, *varianceVar,*maxGradVar, *timeVar;

        //!The number of saved records in the database
        int records;

        //!Write the current state; if the default value of rec=-1 is used, add a new entry
        void writeNetwork(std::vector<double> &params, double error, double cost,
                                 double variance, double maxGradientComponent,double time = -1.0, int rec = -1);
        //Read the rec state of the database. If geometry = true, call computeGeomerty routines (instead of just reading in the d.o.f.s)
        void readNetwork(std::vector<double> &params, int rec){};

    protected:
        void setDimVar();
        void getDimVar();
    };

#endif
