#ifndef TIMER_H
#define TIMER_H

#include <vector>

using namespace std;

class timer
    {
    public:
        timer(){};

        clock_t t1,t2;

        void start(){t1=clock();};
        void stop(){t2=clock();};

        double timeInSeconds(){return (t2-t1)/(double)CLOCKS_PER_SEC;};
        void printWithMessage(std::string message)
            {
            cout << message << "\t" << timeInSeconds() << " seconds" << endl;
        };
    };
#endif
