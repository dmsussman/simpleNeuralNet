#include "readMNIST.h"

int reverseInt(int i)
    {
    unsigned char ch1, ch2, ch3, ch4;
    ch1 = i & 255;
    ch2 = (i >> 8) & 255;
    ch3 = (i >> 16) & 255;
    ch4 = (i >> 24) & 255;
    return((int) ch1 << 24) + ((int)ch2 << 16) + ((int)ch3 << 8) + ch4;
    }

void readMnist(string filename, vector<double> &vec)
    {
    ifstream file(filename, ios::binary);
    if (file.is_open())
        {
        int magicNumber = 0;
        int nImages = 0;
        int nRows = 0;
        int nCols = 0;
        file.read((char*) &magicNumber, sizeof(magicNumber));
        magicNumber = reverseInt(magicNumber);
        file.read((char*) &nImages,sizeof(nImages));
        nImages=reverseInt(nImages);
        file.read((char*) &nRows,sizeof(nRows));
        nRows = reverseInt(nRows);
        file.read((char*) &nCols,sizeof(nCols));
        nCols = reverseInt(nCols);
        vec.resize(nImages*nRows*nCols);
        int idx = 0;
        for (int ii = 0; ii < nImages; ++ii)
            {
            vector<double> tp;
            for (int rr = 0; rr < nRows; ++rr)
                for (int cc = 0; cc< nCols; ++cc)
                    {
                        unsigned char temp = 0;
                        file.read((char*) &temp, sizeof(temp));
                        vec[idx]=(double)temp;
                        idx+=1;
            //            tp.push_back((double)temp);
                    }
            //vec.push_back(tp);
            }
        }
    }

void readMnistLabels(string filename, vector<double> &vec)
    {
    ifstream file(filename, ios::binary);
    if (file.is_open())
        {
        int magicNumber = 0;
        int nImages = 0;
        int nRows = 0;
        int nCols = 0;
        int nDigits = 10;
        file.read((char*) &magicNumber, sizeof(magicNumber));
        magicNumber = reverseInt(magicNumber);
        file.read((char*) &nImages,sizeof(nImages));
        nImages=reverseInt(nImages);
        vector<double> temporary(nImages*nDigits,0.0);
        vec = temporary;
        for (int ii = 0; ii < nImages; ++ii)
            {
            unsigned char temp = 0;
            file.read((char*) &temp, sizeof(temp));
            vec[ii*nDigits+(int)temp] = 1.0;
            }
        }
    }
