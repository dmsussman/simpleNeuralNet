#ifndef BASEOPTIMIZER_H
#define BASEOPTIMIZER_H

#include "std_include.h"

using namespace std;

//the base optimizer will just provide simple gradient descent, I guess
class baseOptimizer
    {
    public:
        baseOptimizer(int N,double learningRate, double L2Regularize = 0.0);

        virtual void update();

        int degreesOfFreedom;
        vector<double> x;
        vector<double> gradient;
        double eta;
        double l2Regularize;

    };

#endif
