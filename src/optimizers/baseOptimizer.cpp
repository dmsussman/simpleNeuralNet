#include "baseOptimizer.h"
#include "functions.h"

baseOptimizer::baseOptimizer(int N,double learningRate,double L2Regularize)
    : degreesOfFreedom(N), eta(learningRate), l2Regularize(L2Regularize)
    {
    x.resize(degreesOfFreedom);
    gradient.resize(degreesOfFreedom);
    }

void baseOptimizer::update()
    {
    vectorSum(x,gradient,-eta);
    if(l2Regularize>0.)
        vectorSum(x,x,-l2Regularize);
    }
